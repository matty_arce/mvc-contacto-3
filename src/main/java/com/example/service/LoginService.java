package com.example.service;

public interface LoginService {

	boolean login(String user, String password);
}
